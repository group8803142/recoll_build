%global         gsspver 1.1.1
%global         __cmake_in_source_build 1

Summary:        Desktop full text search tool with Qt GUI
Name:           recoll
Version:        1.39.1
Release:        1%{?dist}
License:        GPLv2+
URL:            https://www.recoll.org/
Source0:        https://www.recoll.org/recoll-%{version}.tar.gz
BuildRequires:  meson
BuildRequires:  bison
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  ninja-build
BuildRequires:  file-devel
BuildRequires:  aspell-devel
BuildRequires:  chmlib-devel
BuildRequires:  desktop-file-utils
BuildRequires:  extra-cmake-modules
# kio
BuildRequires:  kdelibs4-devel
BuildRequires:  kf5-kio-devel
# krunner
BuildRequires:  kf5-ki18n-devel
BuildRequires:  kf5-krunner-devel
BuildRequires:  kf5-knotifications-devel
BuildRequires:  kf5-kpackage-devel

BuildRequires:  libxslt-devel
BuildRequires:  python-rpm-macros
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  qt5-linguist
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtwebkit-devel
BuildRequires:  xapian-core-devel
BuildRequires:  zlib-devel
Requires:       xdg-utils
Requires:       hicolor-icon-theme
Requires:       %{name}-libs = %{version}-%{release}

%description
Recoll is a personal full text search package for Linux, FreeBSD and
other Unix systems. It is based on a very strong back end (Xapian), for
which it provides an easy to use, feature-rich, easy administration
interface.

%package       libs
Summary:       Libraries for Recoll applications
%description   libs
Shared libraries required to run Recoll applications.

%package       devel
Summary:       Libraries and header files to develop Recoll enabled applications
Requires:      %{name}-libs = %{version}-%{release}
%description   devel
Libraries and header files required to develop Recoll enabled
applications.

%prep
#%setup -q -D -a 1
%setup -q -D
%autopatch -p1

# meson 调用 qmkmk.sh，qmkmk.sh 读取 QMAKE环境变量
# Fedora 官方的qmake-qt6.sh 可以正确设置编译参数，就不需要手动修改pro文件了
#sed -i "s|VPATH = @srcdir@|VPATH = @srcdir@\n\nQMAKE_CXXFLAGS +=%{optflags}|" qtgui/recoll.pro.in

# qmake 并未能正确读取PREFIX环境变量(fixed in 1.38.2)
# sed -i "s|PREFIX = /usr/local| PREFIX = %{_prefix} |" qtgui/recoll.pro.in

%build
export QMAKE=qmake-qt5.sh
%meson -Dwebkit=true -Drecollq=true
%meson_build

%install
%meson_install

desktop-file-install --delete-original \
  --dir=%{buildroot}/%{_datadir}/applications \
  %{buildroot}/%{_datadir}/applications/%{name}-searchgui.desktop

%py_byte_compile %{__python3} %{buildroot}%{_datadir}/%{name}/filters/

%files
%license COPYING
%doc ChangeLog README
%{_bindir}/recoll
%{_bindir}/recollindex
%{_bindir}/recollq
%{_datadir}/recoll
%{_datadir}/metainfo/org.recoll.recoll.appdata.xml
%{_datadir}/applications/recoll-searchgui.desktop
%{_datadir}/icons/hicolor/48x48/apps/recoll.png
%{_datadir}/pixmaps/recoll.png
%{python3_sitearch}/recoll
%{python3_sitearch}/recollchm
#%{python3_sitearch}/Recoll-*.egg-info
#%{python3_sitearch}/recollchm-*.egg-info
%{python3_sitearch}/recollaspell.cpython-*-linux-gnu*.so
#%{python3_sitearch}/recoll_aspell_python_py3-*.egg-info
%{_mandir}/man1/recoll.1*
%{_mandir}/man1/recollq.1*
%{_mandir}/man1/recollindex.1*
#{_mandir}/man1/rclgrep.1*
%{_mandir}/man1/xadump.1*
%{_mandir}/man5/recoll.conf.5*
%{_unitdir}/recollindex@.service
%{_userunitdir}/recollindex.service

%files libs
%{_libdir}/librecoll.so.*

%files devel
%{_libdir}/librecoll.so
%{_includedir}/recoll

%changelog
* Sun Jun 16 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 1.39.1-1
- 1.39.1

* Mon May 27 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 1.38.2-3
- update zh_CN translation
- fix translation load error
- fix icon not shown on gnome taskbar
- built on copr with a local srpm rather than spec

* Fri May 24 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 1.38.2-2
- Switch back to Qt5 + qtwebkit

* Fri May 24 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 1.38.2-1
- 1.38.2

* Tue May 21 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 1.38.1-1
- 1.38.1
